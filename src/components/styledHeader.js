import styled from "styled-components";

const StyledButton = styled.button`
    width: 50px;
    height: 50px;
    background-color: white;
    border: 0.5px solid white;
    ${props => props.type && props.type === "normal" ?
        `color: black;` :
        `color: rgb(175,182,196);`
    }
`

export default StyledButton;